# __Hello__
This is a repo to practice your git. Try and add your name to the list!

`- [Yourname](your gitlab profile url)`

feel free to use these emojis: https://gist.github.com/rxaviers/7360908 :relaxed:

Learning Source: https://faradars.org/courses/fvgit9609-managed-distributed-edition-using-git :v:

# People who know git (more or less)
- [__Jadi__ :heart:](https://jadi.net)
- [__Fakhamatia__ :metal:](https://gitlab.com/fakhamatia)
